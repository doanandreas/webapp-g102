from django import forms

class SolidColorPicker(forms.Form):
    # Color in RGB
    color = forms.CharField(label='Color (in RRGGBB)', max_length=6)