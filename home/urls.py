from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name="index"),
    path('solid', views.setSolid, name="solid"),
    path('cycle', views.setCycle, name="cycle"),
    path('breathing', views.setBreathing, name="breathing"),
    path('off', views.setOff, name="off")
]