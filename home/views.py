from django.shortcuts import render, redirect
from .g203_led import send_mouse_config

# Create your views here.
def index(request):
    return render(request, 'index.html')

# Solid mode arguments:
# 1. Color (RRGGBB)
def setSolid(request):
    color = request.POST['color-value'][1:]
    send_mouse_config('Solid', [color])

    arguments = {
        'colorValue': color,
        'type': "solid"
    }

    return render(request, 'index.html', arguments)

# Cycle mode arguments:
# 1. Brightness (0-100, default 100)
# 2. Cycle rate (100ms - 60000ms, default 10000ms)
def setCycle(request):
    brightness = request.POST['brightness']
    cycle_rate = request.POST['cycle-rate']
    send_mouse_config('Cycle', [cycle_rate, brightness])

    arguments = {
        'brightness': brightness,
        'cycle_rate': cycle_rate,
        'type': "cycle"
    }

    return render(request, 'index.html', arguments)

# Breathing mode arguments:
# 1. Color (RRGGBB)
# 2. Brightness (0% - 100%, default 100)
# 3. Cycle rate (100ms - 60000ms, default 10000ms)
def setBreathing(request):
    color = request.POST['color-value'][1:]
    brightness = request.POST['brightness']
    cycle_rate = request.POST['cycle-rate']
    send_mouse_config('Breathing', [color, cycle_rate, brightness])

    arguments = {
        'colorValue': color,
        'brightness': brightness,
        'cycle_rate': cycle_rate,
        'type': "breathing"
    }

    return render(request, 'index.html', arguments)

def setOff(request):
    send_mouse_config('Solid', ["000000"])

    arguments = {
        'type': "off"
    }

    return render(request, 'index.html', arguments)